import express from 'express'

class App {
  app = express()

  static instance = new App()
  
  static listen(port: number) {
    this.instance.app.listen(port, () => {
      console.log('端口启动成功')
    })
  }
}

export default App
